package game;

public class TimerEvent implements Runnable {
    private Score score;
    private int decreaseValue;

    public TimerEvent(Score score, int value) {
        this.score = score;
        this.decreaseValue = value;
    }

    @Override
    public void run() {
        score.add(-decreaseValue);
    }
}
