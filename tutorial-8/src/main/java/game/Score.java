package game;

import java.util.concurrent.atomic.AtomicInteger;

public class Score {

    // Thread safe integer.
    private AtomicInteger score;

    public Score() {
        score = new AtomicInteger(0);
    }

    public void add(int num) {
        score.addAndGet(num);
    }

    public void addPercentage(double percentage) {
        score.getAndUpdate(x -> x + (int)(percentage * x));
    }

    public int value() {
        return score.get();
    }

    public void set(int num) {
        score.set(num);
    }
}