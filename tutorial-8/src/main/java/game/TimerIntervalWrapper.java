package game;

public class TimerIntervalWrapper {
    private TimerEvent event;
    private int intervalInSeconds;

    public TimerIntervalWrapper(TimerEvent event, int intervalInSeconds) {
        this.event = event;
        this.intervalInSeconds = intervalInSeconds;
    }

    public TimerEvent getEvent() {
        return this.event;
    }

    public int getIntervalInSeconds() {
        return intervalInSeconds;
    }
}
