package game;

import java.util.List;
import java.util.Vector;

public class EventLoop extends Thread {

    // Thread-safe list.
    private List<TimerIntervalWrapper> events;
    private int timeCounter;
    private boolean running;

    public EventLoop() {
        events = new Vector<>();
        timeCounter = 0;
        running = false;
    }

    public void addEvent(TimerEvent event, int intervalInSeconds) {
        events.add(new TimerIntervalWrapper(event, intervalInSeconds));
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(1000);
                timeCounter++;

                for (TimerIntervalWrapper event : events) {
                    if (timeCounter % event.getIntervalInSeconds() == 0) {
                        event.getEvent().run();
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void start() {
        this.running = true;
        super.start();
    }

    public void safeStop() {
        this.running = false;
    }

}