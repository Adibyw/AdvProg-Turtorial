package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {
    private AtomicInteger counter = new AtomicInteger(0);

    public void increment() {
        counter.getAndIncrement();
    }

    public void decrement() {
        counter.getAndDecrement();
    }

    public int value() {
        return counter.get();
    }
}