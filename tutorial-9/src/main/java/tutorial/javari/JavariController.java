package tutorial.javari;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@RestController
public class JavariController {

    @Autowired
    JavariDataBase dataBase;

    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    @ResponseBody
    public List<Animal> getAllAnimals() throws IOException {
        if (!dataBase.isAccesed()) {
            dataBase.readCSVfirstTime();
            dataBase.setAccesed(true);
        }

        return dataBase.getMap();
    }

    @RequestMapping(value = "/javari/{ID}", method = RequestMethod.GET)
    @ResponseBody
    public Animal getSelectedAnimal(@PathVariable("ID") String ID) throws IOException {
        getAllAnimals();
        Animal selected = dataBase.getMap()
                                .stream()
                                .filter(animal ->
                                        Integer.parseInt(ID) == animal.getId())
                                .findFirst()
                                .orElse(null);

        if(selected != null) {
            return selected;
        }
        else{
            throw new RuntimeException("Not Found");
        }
    }

    @RequestMapping(value = "/javari/{ID}", method = RequestMethod.DELETE)
    @ResponseBody
    public Animal delAnimal(@PathVariable("ID") String ID) throws IOException {
        Animal deleted =
                dataBase.getMap()
                        .stream()
                        .filter(animal -> Integer.parseInt(ID) == animal.getId())
                        .findFirst()
                        .orElse(null);
        if(deleted != null) {
            dataBase.getMap().remove(deleted);
            dataBase.writeCSV();
            return deleted;
        }
        else{
            throw new RuntimeException("Not Found");
        }
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    @ResponseBody
    public void addAnimal(@RequestBody String json) throws IOException {
        HashMap<String, Object> result = new ObjectMapper().readValue(json, HashMap.class);
        Animal check =
                dataBase.getMap()
                        .stream()
                        .filter(animal -> (int)result.get("id") == animal.getId())
                        .findFirst()
                        .orElse(null);

        if(check == null){
            Animal animal =  new Animal((int)result.get("id")
                    , result.get("type").toString()
                    , result.get("name").toString()
                    , Gender.parseGender(result.get("gender").toString().toLowerCase())
                    , Double.parseDouble(result.get("length").toString())
                    , Double.parseDouble(result.get("weight").toString())
                    , Condition.parseCondition(result.get("condition").toString().toLowerCase()));

            dataBase.addAnimal(animal);
            dataBase.writeCSV();
        }
        else{
            throw new RuntimeException("ID sudah ada");
        }
    }
}
