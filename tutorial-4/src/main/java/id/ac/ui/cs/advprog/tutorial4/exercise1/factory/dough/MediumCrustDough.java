package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class MediumCrustDough implements Dough{

    public String toString(){return "Medium Crust Dough";}
}
