package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MediumCrustDoughTest {
    private MediumCrustDough mediumCrustDough;

    @Before
    public void setUp(){
        mediumCrustDough = new MediumCrustDough();
    }

    @Test
    public void testToString(){
        assertEquals("Medium Crust Dough", mediumCrustDough.toString());
    }
}