package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BBQSauceTest {
    private BBQSauce bbqSauce;

    @Before
    public void setUp() {
        bbqSauce = new BBQSauce();
    }

    @Test
    public void testToString(){
        assertEquals("BBQ Sauce", bbqSauce.toString());
    }
}
