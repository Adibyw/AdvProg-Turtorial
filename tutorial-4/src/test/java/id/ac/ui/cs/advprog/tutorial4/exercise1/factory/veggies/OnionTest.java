package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class OnionTest {

    private Onion onion;

    @Before
    public void setUp() {
        onion = new Onion();
    }

    @Test
    public void testToString() {
        assertEquals("Onion", onion.toString());
    }
}