package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class VeggiePizzaTest {
    private Class<?> VeggiePizza;
    private VeggiePizza veggiePizzaNY;
    private VeggiePizza veggiePizzaDepok;

    @Before
    public void setUp() throws Exception{
        VeggiePizza = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza");

        veggiePizzaNY = new VeggiePizza(new NewYorkPizzaIngredientFactory());
        veggiePizzaNY.prepare();

        veggiePizzaDepok = new VeggiePizza(new DepokPizzaIngredientFactory());
        veggiePizzaDepok.prepare();
    }

    @Test
    public void testCheesePizzaIsAPizza(){
        Class<?> parent = VeggiePizza.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
    }

    @Test
    public void testPreparePizza(){
        assertNotEquals(null, veggiePizzaNY.dough);
        assertNotEquals(null, veggiePizzaNY.sauce);
        assertNotEquals(null, veggiePizzaNY.cheese);
        assertNotEquals(null, veggiePizzaNY.veggies);


        assertNotEquals(null, veggiePizzaDepok.dough);
        assertNotEquals(null, veggiePizzaDepok.sauce);
        assertNotEquals(null, veggiePizzaDepok.cheese);
        assertNotEquals(null, veggiePizzaDepok.veggies);

    }

    @Test
    public void testToString(){
        veggiePizzaNY.setName("New York Style Veggie Pizza");

        String str = "---- New York Style Veggie Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Garlic, Onion, Mushrooms, Red Pepper\n";

        assertEquals(str,veggiePizzaNY.toString());

        veggiePizzaDepok.setName("Depok Style Veggie Pizza");

        str = "---- Depok Style Veggie Pizza ----\n" +
                "Medium Crust Dough\n" +
                "BBQ Sauce\n" +
                "Cheddar Cheese\n" +
                "Black Olives, Eggplant, Spinach, Tomato\n";

        assertEquals(str, veggiePizzaDepok.toString());
    }
}
