package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BoiledClamTest {
    private BoiledClams boiledClams;

    @Before
    public void setUp() {
        boiledClams = new BoiledClams();
    }

    @Test
    public void testToString() {
        assertEquals("Boiled Clams from Lombok Island", boiledClams.toString());
    }
}
