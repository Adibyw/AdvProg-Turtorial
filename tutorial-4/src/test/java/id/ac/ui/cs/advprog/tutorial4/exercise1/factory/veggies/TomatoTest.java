package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TomatoTest {
    private Tomato tomato;

    @Before
    public void serUp(){
        tomato = new Tomato();
    }

    @Test
    public void testToString(){
        assertEquals("Tomato", tomato.toString());
    }
}
