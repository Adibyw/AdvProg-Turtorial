package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void createPizzaTest() {
        Pizza pizza;

        pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());

        pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());

        pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }

    @Test
    public void createPizzaNullTest() {
        Pizza pizza = newYorkPizzaStore.createPizza("null");
        assertNull(pizza);
    }
}