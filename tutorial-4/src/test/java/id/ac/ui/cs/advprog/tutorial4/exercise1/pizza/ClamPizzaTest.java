package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class ClamPizzaTest {
    private Class<?> ClamPizza;
    private ClamPizza clamPizzaNY;
    private ClamPizza clamPizzaDepok;

    @Before
    public void setUp() throws Exception{
        ClamPizza = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza");

        clamPizzaNY = new ClamPizza(new NewYorkPizzaIngredientFactory());
        clamPizzaNY.prepare();

        clamPizzaDepok = new ClamPizza(new DepokPizzaIngredientFactory());
        clamPizzaDepok.prepare();
    }

    @Test
    public void testCheesePizzaIsAPizza(){
        Class<?> parent = ClamPizza.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
    }

    @Test
    public void testPreparePizza(){
        assertNotEquals(null, clamPizzaNY.dough);
        assertNotEquals(null, clamPizzaNY.sauce);
        assertNotEquals(null, clamPizzaNY.cheese);
        assertNotEquals(null, clamPizzaNY.clam);


        assertNotEquals(null, clamPizzaDepok.dough);
        assertNotEquals(null, clamPizzaDepok.sauce);
        assertNotEquals(null, clamPizzaDepok.cheese);
        assertNotEquals(null, clamPizzaDepok.clam);

    }

    @Test
    public void testToString(){
        clamPizzaNY.setName("New York Style Clam Pizza");

        String str = "---- New York Style Clam Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n" +
                "Fresh Clams from Long Island Sound\n";

        assertEquals(str,clamPizzaNY.toString());

        clamPizzaDepok.setName("Depok Style Clam Pizza");

        str = "---- Depok Style Clam Pizza ----\n" +
                "Medium Crust Dough\n" +
                "BBQ Sauce\n" +
                "Cheddar Cheese\n" +
                "Boiled Clams from Lombok Island\n";

        assertEquals(str, clamPizzaDepok.toString());
    }
}
