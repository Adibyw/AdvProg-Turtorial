package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
    private Class<?> CheesePizza;
    private CheesePizza cheesePizzaNY;
    private CheesePizza cheesePizzaDepok;

    @Before
    public void setUp() throws Exception{
        CheesePizza = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza");

        cheesePizzaNY = new CheesePizza(new NewYorkPizzaIngredientFactory());
        cheesePizzaNY.prepare();

        cheesePizzaDepok = new CheesePizza(new DepokPizzaIngredientFactory());
        cheesePizzaDepok.prepare();
    }

    @Test
    public void testCheesePizzaIsAPizza(){
        Class<?> parent = CheesePizza.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
    }

    @Test
    public void testPreparePizza(){
        assertNotEquals(null, cheesePizzaNY.dough);
        assertNotEquals(null, cheesePizzaNY.cheese);
        assertNotEquals(null, cheesePizzaNY.sauce);

        assertNotEquals(null, cheesePizzaDepok.dough);
        assertNotEquals(null, cheesePizzaDepok.cheese);
        assertNotEquals(null, cheesePizzaDepok.sauce);
    }

    @Test
    public void testToString(){
        cheesePizzaNY.setName("New York Style Cheese Pizza");
        String str = "---- New York Style Cheese Pizza ----\n" +
                "Thin Crust Dough\n" +
                "Marinara Sauce\n" +
                "Reggiano Cheese\n";

        assertEquals(str,cheesePizzaNY.toString());

        cheesePizzaDepok.setName("Depok Style Cheese Pizza");
        str = "---- Depok Style Cheese Pizza ----\n" +
                "Medium Crust Dough\n" +
                "BBQ Sauce\n" +
                "Cheddar Cheese\n";

        assertEquals(str, cheesePizzaDepok.toString());
    }
}
