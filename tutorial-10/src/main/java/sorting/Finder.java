package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    /**
     * Binary Search Algorithm with O(Log n) average complexity.
     * This algorithm can only search a value if the sequence is already sorted.
     *
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @param low Starting index.
     * @param high End index.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int binarySearch(int[] arrOfInt, int searchedValue,
                                   int low, int high) {
        if (low <= high) {
            int mid = low + (high - low) / 2;

            // If the element is present at the
            // middle itself
            if (arrOfInt[mid] == searchedValue) {
                return arrOfInt[mid];
            }

            // If element is smaller than mid, then
            // it can only be present in left subarray
            if (arrOfInt[mid] > searchedValue) {
                return binarySearch(arrOfInt, searchedValue, low, mid - 1);
            }

            // Else the element can only be present
            // in right subarray
            return binarySearch(arrOfInt, searchedValue, mid + 1, high);
        }

        return -1;
    }
}
