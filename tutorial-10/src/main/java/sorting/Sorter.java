package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    /** A Method used for quick sort algorithm.
     * This method takes the middle element as a pivot and place it at its correct spot
     * If an element is smaller than pivot, put it to the left of pivot
     * If an element is bigger than pivot, put it to the right of pivot
     * After the method is done with the current pivot, it will do a recursive
     * call and changed the pivot
     * @param list array of integer that need to be sorted.
     * @param pointerLow Starting index.
     * @param pointerHigh Ending index.
     * @return a sorted array of integer.
     */
    public static int[] quickSort (int[] list, int pointerLow, int pointerHigh) {
        if (pointerLow == pointerHigh){
            return list;
        }
        int low = pointerLow, high = pointerHigh;
        int titikTengah = (pointerHigh - pointerLow + 1) / 2;
        int pivot = list[pointerLow + titikTengah];

        while (low <= high) {
            while (list[low] < pivot) {
                low++;
            }
            while (pivot < list[high]) {
                high--;
            }
            if (low <= high) {
                int temp = list[low];
                list[low] = list[high];
                list[high] = temp;
                low++;
                high--;
            }
        }
        if (pointerLow < high) {
            quickSort(list, pointerLow, high);
        }
        if (low < pointerHigh){
            quickSort(list, low, pointerHigh);
        }

        return list;
    }
}
