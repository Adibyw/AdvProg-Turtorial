package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {

    String pathFile;
    int numberOfItemToBeSorted;
    int[] sequenceInput;

    @Before
    public void setUp() throws IOException {
        String pathFile = "plainTextDirectory/input/sortingProblem.txt";
        int numberOfItemToBeSorted = 50000;

        sequenceInput = Main.convertInputFileToArray();
    }

    @Test
    public void testSortingAlgorithm() {
        int[] sortedInput = Sorter.quickSort(sequenceInput, 0,
                                            sequenceInput.length - 1);

        for (int i = 0; i < sortedInput.length - 2; i++) {
            assertTrue(sortedInput[i] < sortedInput[i + 1]);
        }

    }

    @Test
    public void testSearchingAlgorithm() {
        int searchingResult = Finder.slowSearch(sequenceInput, 40738);
        assertEquals(40738, searchingResult);

        int[] sortedInput = Sorter.quickSort(sequenceInput, 0, sequenceInput.length - 1);

        searchingResult = Finder.binarySearch(sortedInput, 40738, 0, sortedInput.length - 1);
        assertEquals(40738, searchingResult);
    }
}
