package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.List;

public class TutupLapak {
    public static void main(String[] args){
        Company tutupLapak = new Company();

        Employees employee = new Ceo("Luffy", 500000.00);
        tutupLapak.addEmployee(employee);

        employee = new Cto("Zorro", 320000.00);
        tutupLapak.addEmployee(employee);

        employee = new BackendProgrammer("Franky", 94000.00);
        tutupLapak.addEmployee(employee);

        employee = new FrontendProgrammer("Nami",66000.00);
        tutupLapak.addEmployee(employee);

        employee = new UiUxDesigner("sanji", 177000.00);
        tutupLapak.addEmployee(employee);

        employee = new NetworkExpert("Brook", 83000.00);
        tutupLapak.addEmployee(employee);

        employee = new SecurityExpert("Chopper", 70000.00);
        tutupLapak.addEmployee(employee);

        List<Employees> employeesList = tutupLapak.getAllEmployees();
        for (Employees employees: employeesList) {
            System.out.println(employees.getName() + " " + employees.getRole() +
                    " Salary = $" + employees.getSalary());
        }
        System.out.println("Net Salaries = $" + tutupLapak.getNetSalaries());

    }
}
