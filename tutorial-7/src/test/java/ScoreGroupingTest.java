import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    private Map<String, Integer> scores;
    private Map<Integer, List<String>> groupedByScores;

    @Before
    public void setUp() {
        scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        groupedByScores = ScoreGrouping.groupByScores(scores);
    }

    @Test
    public void testGroupingByScoresCouldDivideScoresProperly() {
        assertEquals(3, groupedByScores.size());
    }

    @Test
    public void testGroupingByScoresCouldDivideNameProperly() {
        assertEquals(Arrays.asList("Charlie", "Foxtrot"), groupedByScores.get(11));
        assertEquals(Arrays.asList("Alice"), groupedByScores.get(12));
        assertEquals(Arrays.asList("Emi", "Bob", "Delta"), groupedByScores.get(15));
    }
}