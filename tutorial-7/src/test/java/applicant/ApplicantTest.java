package applicant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    private Applicant applicant;
    private Predicate<Applicant> creditEvaluator;
    private Predicate<Applicant> qualifiedEvaluator;
    private Predicate<Applicant> employmentEvaluator;
    private Predicate<Applicant> crimeCheck;

    @Before
    public void setUp() {
        applicant = new Applicant();

        creditEvaluator = anApplicant -> anApplicant.getCreditScore() > 600;
        qualifiedEvaluator = Applicant::isCredible;
        employmentEvaluator = anApplicant -> anApplicant.getEmploymentYears() > 0;
        crimeCheck = anAplicant -> !anAplicant.hasCriminalRecord();
    }

    @Test
    public void testApplicantIsCredible() {
        assertTrue(applicant.isCredible());
    }

    @Test
    public void testApplicantGetCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void testApplicantGetEmploymentYear() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void testApplicantHasCriminalRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testEvaluateTrue() {
        assertTrue(Applicant.evaluate(applicant, creditEvaluator));
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator));
        assertTrue(Applicant.evaluate(applicant, employmentEvaluator));

        assertTrue(Applicant.evaluate(applicant, creditEvaluator.and(qualifiedEvaluator)));
        assertTrue(Applicant.evaluate(applicant, creditEvaluator.and(qualifiedEvaluator)
                                                                .and(employmentEvaluator)));
    }

    @Test
    public void testEvaluateFalse() {
        assertFalse(Applicant.evaluate(applicant, crimeCheck));

        assertFalse(Applicant.evaluate(applicant, creditEvaluator.and(crimeCheck)));
        assertFalse(Applicant.evaluate(applicant, creditEvaluator.and(qualifiedEvaluator)
                                                                 .and(employmentEvaluator)
                                                                 .and(crimeCheck)));
    }
}
